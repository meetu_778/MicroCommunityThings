package com.java110.core.util;

import java.text.DecimalFormat;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @ClassName SeqUtil
 * @Description TODO
 * @Author wuxw
 * @Date 2020/5/20 21:36
 * @Version 1.0
 * add by wuxw 2020/5/20
 **/
public class SeqUtil {

    private static int MACHINE_SEQ = 1;


    private static final long ONE_STEP = 1000000;
    private static final Lock LOCK = new ReentrantLock();
    private static short lastCount = 1;
    private static int count = 0;

    /**
     * 获取流水
     *
     * @return
     */
    public static String getId() {
        return UUID.randomUUID().toString().replace("-","");
    }

    public static String nextId(String idLength) {
        LOCK.lock();
        try {
            if (lastCount == ONE_STEP) {
                lastCount = 1;
            }
            count = lastCount++;
        } finally {
            LOCK.unlock();
            String id = getRandom() + String.format(idLength, count);
            id = id.replace("-","");
            return id;
        }
    }

    /**
     * 获取随机数
     *
     * @return
     */
    private static String getRandom() {
        Random random = new Random();
        String result = "";
        for (int i = 0; i < 4; i++) {
            result += random.nextInt(10);
        }
        return result;
    }
    /**
     * pgId生成
     *
     * @return
     */
    public static String getGeneratorId()  {
            return DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_H) + nextId("%04d");
    }

    public static synchronized String getMachineSeq() {
        MACHINE_SEQ++;
        DecimalFormat decimalFormat = new DecimalFormat("000");
        String machineSeq = DateUtil.getCurrentDay() + "" + DateUtil.getCurrentMin() + "" + decimalFormat.format(MACHINE_SEQ);
        return machineSeq;
    }

    public static void main(String[] args) {
        System.out.printf(getMachineSeq());
    }
}
